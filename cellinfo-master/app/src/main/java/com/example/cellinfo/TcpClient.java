package com.example.cellinfo;

/**
 * Author: Jan Plachy (plachyjan@gmail.com)
 */
import android.util.Log;

import com.google.android.gms.common.util.Hex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    public String SERVER_IP = "192.168.1.8"; //server IP address 192.168.1.8
    public int SERVER_PORT = 1234;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;

    private boolean mMessageEnd = false;
    private int mServerMessageLength = 0;
    private int mTotalBytesRead = 0;
    private DataInputStream mDataInputStreamdIn;
    private DataOutputStream mDataOutputStreamdOut;
    private String mReceivedMessage;
    private boolean mSocketInitialized = false;
    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient(OnMessageReceived listener,String serverIP, int serverPort){
        SERVER_IP = serverIP;
        SERVER_PORT = serverPort;
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(final String message) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mBufferOut != null) {
                    Log.d(TAG, "Sending: " + message);
                    mBufferOut.println(message);
                    mBufferOut.flush();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void sendMessageBytes(final String message) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mBufferOut != null) {
                    Log.d(TAG, "Sending: " + message);
                    mBufferOut.println(message);
                    mBufferOut.flush();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public void configure(String serverIP, int serverPort){
        SERVER_IP = serverIP;
        SERVER_PORT = serverPort;
    }

    public void run() {

        mRun = true;
        boolean connectionEstablished = false;

        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            Log.d("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVER_PORT);
            try {
                Log.d("TCP Client", "Try");
                //sends the message to the server
               // mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                mDataInputStreamdIn = new DataInputStream(socket.getInputStream());
                mDataOutputStreamdOut= new DataOutputStream(socket.getOutputStream());

                //receives the message which the server sends back
               // mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                sendStringByte("HELLO"); // init communication

                //in this while the client listens for the messages sent by the server
                Log.d("TCP Client", "Send msg");
                while (mRun) {
                    receiveMessage();
                    if(mReceivedMessage!=""&& mMessageListener != null){
                        if(mReceivedMessage.contains("210 Hello")){
                            mSocketInitialized = true;
                        }
                        Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mReceivedMessage + "'");
                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(mReceivedMessage);

                    }
                }

                Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

            } catch (IOException e) {
                Log.e("TCP", "S: Error", e);
            } catch (Exception e) {
                Log.e("TCP", "S: Error", e);
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }

    public void sendStringByte (String message){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                byte[] msg = message.getBytes();
                try {
                    mDataOutputStreamdOut.writeInt(msg.length); // write length of the message
                    mDataOutputStreamdOut.write(msg);           // write the message
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void  receiveMessage(){
        mMessageEnd = false;
        mReceivedMessage = "";
        try {
            mServerMessageLength  = mDataInputStreamdIn.readInt();

        mTotalBytesRead = 0;
        StringBuilder dataString = new StringBuilder(mServerMessageLength);
        byte[] messageByte = new byte[mServerMessageLength];

        while(!mMessageEnd){
            int currentBytesRead = mDataInputStreamdIn.read(messageByte);
            mTotalBytesRead = currentBytesRead + mTotalBytesRead;
            if(mTotalBytesRead <= mServerMessageLength) {
                dataString
                        .append(new String(messageByte, 0, currentBytesRead, StandardCharsets.UTF_8));
            } else {
                dataString
                        .append(new String(messageByte, 0, mServerMessageLength - mTotalBytesRead + currentBytesRead,
                                StandardCharsets.UTF_8));
            }
            if(dataString.length()>=mServerMessageLength) {
                mMessageEnd = true;
                mReceivedMessage = dataString.toString();
            }
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean ismSocketInitialized(){
        return mSocketInitialized;
    }

}
