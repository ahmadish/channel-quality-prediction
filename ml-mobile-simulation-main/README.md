## ML Mobile Simulation

## Description
This simulator generates artificial RSRP values to provide a sufficient amount of data to train Deep Neural Network and enhance its performance.\
Various path loss models were tested to ensure the best similarity between measured and generated data.

## Usage
To start the simulation use the **runSimulation.sh** script. First choose which parts of the process do you wish to run. This is done using these flags:

| flag | option |function |
| ----------- | ----------- | ----------- |
| -s, --simulate | |  generate synthetic RSRP values|
| -a, --augment | \[FILE\] | augment data in \[FILE\] *(has to be in data/txt/)*|
| -p, --process | | process measured data  |
| -t, --train | | start transfer learning |

## Examples
##### Run the whole process:
`$ ./runSimulation.sh -spta datasim.txt` \
Path to *datasim.txt* would be *mlmobile/data/txt/datasim.txt*. However, the script will transfer it here from *ml-mobile-simulation/data/txt/datasim.txt*.

##### Generate new data and augment them:
`$ ./runSimulation.sh -s --augment datasimnew.txt` \
This can be used when you only need to visualize simulated data eg. when testing new pathloss model.

##### Transfer learning with unchanged measured data:
`$ ./runSimulation.sh -sta datasim.txt` \
When using the same data from processMobile.

##### Only transfer learning:
`$ ./runSimulation.sh --train` \
When adjusting the DNN settings.
