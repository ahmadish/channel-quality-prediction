from audioop import mul
from distutils.log import error
import math
import numpy as np
from scipy import interpolate
class channel:
    def __init__(self, model = 'freeSpace', frequency = 2.6e6,txPowerDb = 23, mu = 0, sigma = 3,shadowOn = True, room_scale = 1, step_interp = 0.1):
        self.model = model
        self.frequency = frequency # in Hz
        self.wall_room_attenuation = []
        self.room_shadowing = []
        self.mu = mu
        self.sigma = sigma
        self.shadowOn = shadowOn
        self.room_scale = room_scale
        self.step_interp =  step_interp 
        
    def dist(self,loc1,loc2):
       
        return math.sqrt(math.pow(abs((loc1[0]/self.room_scale-loc2[0]/self.room_scale)),2)+math.pow(abs((loc1[1]/self.room_scale-loc2[1]/self.room_scale)),2)+math.pow(abs((loc1[2]/self.room_scale-loc2[2]/self.room_scale)),2))

    def pathloss(self,distance = 0 ,los = 0,bs_heigh = 1,ue_height = 1,rm1 = -1, rm2 = -1, ue_location = [0,0]):
        PL = 0
        PL_wall = 0
        PL_shadow = 0
        if self.model == 'freeSpace':
            if los:
                PL = 20*math.log10(distance) + 20*math.log10(self.frequency) -147.55
            else:
                # add NLOS PL 
                PL = 20*math.log10(distance) + 20*math.log10(self.frequency) -147.55

        elif self.model =='3GPPindoor': # 3GPP TR 38.901 version 14.3.0 Release 14
            PLlos = 17.3*math.log10(distance) + 20*math.log10(self.frequency/1e6) +32.4

            if los:
                PL = PLlos
            else:
                PLnlos = 31.9*math.log10(distance) + 20*math.log10(self.frequency/1e6) +32.4
                PL = max(PLlos,PLnlos)

                
        elif self.model =='3GPPinH-office': # 3GPP TR 38.901 version 14.3.0 Release 14
            PLlos = 32.4+17.3*math.log10(distance) + 20*math.log10(self.frequency/1e6) +32.4

            if los:
                PL = PLlos
            else:
                PLnlos = 38.3*math.log10(distance) + 24.9*math.log10(self.frequency/1e6) +17.30
                PL = max(PLlos,PLnlos)
        else:
            error('path loss model: ' + self.model + ' not implemented') 

        if rm1 != -1 and rm2 != -1:
           PL_wall =  self.get_walls(rm1,rm2)   

        if self.shadowOn:
            PL_shadow =  self.room_shadowing[round((ue_location[0]/self.room_scale)/self.step_interp)][round((ue_location[1]/self.room_scale)/self.step_interp)]
        return PL + PL_wall + PL_shadow

    def get_fast_fading(self):

        return    

    def set_wall_room_attenuation(self,room_attenuation):
        self.wall_room_attenuation = room_attenuation        

    def get_walls(self,rm1,rm2):
        return self.wall_room_attenuation[rm1][rm2]

    def shadowing(self, mu = 0 , sigma = 3, step = 1, room = None):
        epsilon = 2 # generate slightly larger shadowing map to cover locations near walls
        # room wider than longer -> square width x width
        if room == None:
            error('Must input room')
        if room.width > room.length:
             x_min = round(room.xcor/self.room_scale)
             x_max = math.ceil(room.xcor/self.room_scale+room.width/self.room_scale)+epsilon
             y_min = round(room.ycor/self.room_scale)
             y_max = math.ceil(room.ycor/self.room_scale+room.width/self.room_scale)+epsilon
        # room longer than wider -> square length x length
        elif room.width <= room.length:
            x_min = round(room.xcor/self.room_scale)
            x_max = math.ceil(room.xcor/self.room_scale+room.length/self.room_scale)+epsilon
            y_min = round(room.ycor/self.room_scale)
            y_max = math.ceil(room.ycor/self.room_scale+room.length/self.room_scale)+epsilon

        x = np.arange(x_min,x_max,step)
        y = np.arange(y_min,y_max,step)

        #xx, yy = np.meshgrid(x, y)

        # generate shadowing with given mu and sigma
        z = np.random.normal(mu, sigma, [x_max-x_min,y_max-y_min])

        f = interpolate.interp2d(x, y, z, kind='cubic')

        # determine shadowing values on interpolated grid (znew)
        xnew = np.arange(x_min, x_max, self.step_interp)
        ynew = np.arange(y_min, y_max, self.step_interp)

        znew = f(xnew, ynew)
        #self.room_shadowing.append(shadow_map(znew[0:((room.width/self.room_scale)//step_interp), 0:int((room.length/self.room_scale)//step_interp)] , room)) # fix it
        self.room_shadowing = znew[0:int((room.width/self.room_scale)/self.step_interp)+epsilon, 0:int((room.length/self.room_scale)/self.step_interp)+epsilon]

        #return znew[0:(room.width//step_interp), 0:(room.length//step_interp)]    
