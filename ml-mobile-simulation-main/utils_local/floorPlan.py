from distutils.log import error
from classes import room as rm    
import numpy as np
import utils.room as room
from utils import Graph 

class floorPlan():
    def __init__(self, length, width):
        self.length = length
        self.width = width
        self.floor_plan = []
        self.rooms = {}
        self.rooms_adjacency_matrix = {}
        self.adjacency_matrix_rooms = []
        self.attenuation_rooms = []
        self.adjacency_paths = []
        self.attenuation_between_rooms = []
        self.sim_room = room.room(xcor = 0, ycor =  0, width = width, length = length,room_name = 'area',wall_attenutations = [20,20,20,20],wall_thicknesses = [100,500,200,50])
        

    def add_room(self,room, room_id):
        self.rooms[room_id] = room    


    def get_room_from_location(self, location):   

        for rm in self.rooms:
            if (location[0]> rm.xcor) and (location[0]< (rm.xcor+rm.width)) and (location[1]> rm.ycor) and (location[0]< (rm.ycor+rm.length)):
                return rm.room_name        
        return ''

    def get_room_index_from_room_name(self, room_name):   
        room_idx = 0
        for rm in self.rooms:
            if room_name == rm.room_name:
                return room_idx
            else:
                room_idx +=1        
        return -1    

    def create_room (self, room_id):
        return room_id

    def get_walls(self,loc1,loc2):
        # return a vector of valls, with their specifics
        return

    def add_room_adjacency(self,room_id,adjacency):
        self.rooms_adjacency_matrix[room_id] = adjacency

    def determine_room_to_room_attenuation(self):
        # complete from room adjacency matrix by determining lowest attenuation between any two rooms and storing it in 2D array
        self.adjacency_matrix_rooms = [ [0]*len(self.rooms_adjacency_matrix) for i in range(len(self.rooms_adjacency_matrix))]
        self.attenuation_rooms = [ [float("Inf")]*len(self.rooms_adjacency_matrix) for i in range(len(self.rooms_adjacency_matrix))]
        for rm in self.rooms_adjacency_matrix:
            room_idx = self.get_room_index(rm)
            if room_idx != -1:
                self.attenuation_rooms[room_idx][room_idx] = 0
                self.adjacency_matrix_rooms[room_idx][room_idx] = 1
            else:
                error('room ' + rm  + 'not found')

            for adjacency in self.rooms_adjacency_matrix[rm]:
                room_adj_idx = self.get_room_index(adjacency['room'])

                if room_idx != -1 and room_adj_idx != -1:
                    self.attenuation_rooms[room_idx][room_adj_idx] = adjacency['attenuation']
                    self.adjacency_matrix_rooms[room_idx][room_adj_idx] = 1
                else:
                    if room_idx == -1:
                        error('room ' + rm  + 'not found')
                    if room_adj_idx == -1:
                        error('room ' + adjacency['room']  + 'not found')
        return
    
    def get_room_index(self,room):
        room_idx = 0
        for rm in self.rooms:
            if rm.room_name == room:
                return room_idx
            else:
                room_idx += 1    
        return -1

    def room_adjacency_path_pre_calculate(self):
        g = Graph.Graph(len(self.rooms_adjacency_matrix))
        for rm in range(0,len(self.attenuation_rooms)):
            for rm_adj in range(0,len(self.attenuation_rooms)):
                if self.attenuation_rooms[rm][rm_adj]>0:
                    g.add_edge(rm, rm_adj, self.attenuation_rooms[rm][rm_adj])
        self.adjacency_paths = [ [-1]*len(self.rooms_adjacency_matrix) for i in range(len(self.rooms_adjacency_matrix))]

        for rm in range(0,len(self.attenuation_rooms)):
            path,dist  = g.bellman_ford(rm)
            #D = g.dijkstra(rm)
            self.attenuation_between_rooms.append(dist)


    def generate_user_locations(self, user_location_step):   
        ue_location = []
        for rm in self.rooms:
            for x_cor in range(rm.xcor+user_location_step,rm.xcor+rm.width-user_location_step,user_location_step):
                for y_cor in range(rm.ycor+user_location_step,rm.ycor+rm.length-user_location_step,user_location_step):
                    ue_location.append([x_cor,y_cor])
        
        return ue_location 
