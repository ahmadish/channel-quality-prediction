clear all
close all
 
data_all = importdata('data.txt');

t = abs(data_all(:,1))';
x = abs(data_all(:,2:end))';
setdemorandstream(391418381)  % The random seed is set to avoid randomness
vec_nodes=[20,18,15,12,8];  

net =fitnet(vec_nodes);  
net.trainFcn ='trainlm' ; 
net.trainParam.epochs=1000; % Maximum number of epochs to train. The default value is 1000.
net.trainParam.max_fail=10; % Maximum validation failures. The default value is 6. 
[net,tr] = train(net,x,t);
nntraintool
testX = x(:,tr.testInd);
testT = t(:,tr.testInd);
testY = net(testX);
testIndices = vec2ind(testY);

perf = sum(abs( testY - testT));
display(perf/length(testT)) % Mean absolute error 
