#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import loadtxt

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras

np.random.seed(1)
tf.random.set_seed(2)

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)

dataset = loadtxt('data.txt', delimiter=',')
column_names = ['31', '32', '33', '11']

raw_dataset = pd.read_csv('data.txt', names=column_names,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=True)

dataset = raw_dataset.copy()
dataset.tail()
dataset.isna().sum()

train_dataset = dataset.sample(frac=0.8, random_state=0)
test_dataset = dataset.drop(train_dataset.index)
train_dataset.describe().transpose()

train_features = train_dataset.copy()
test_features = test_dataset.copy()

train_labels = train_features.pop('11')
test_labels = test_features.pop('11')

normalizer =  tf.keras.layers.Normalization(axis=-1)
normalizer.adapt(np.array(train_features))
cell1 = np.array(train_features)
print(cell1[:10])
cell1_normalizer = tf.keras.layers.Normalization(input_shape=[3,], axis=None)
cell1_normalizer.adapt(cell1)


dnn_cell_model = keras.Sequential([
    cell1_normalizer,
    layers.Dense(20, activation='relu'),
    layers.Dense(18, activation='relu'),
    layers.Dense(15, activation='relu'),
    layers.Dense(12, activation='relu'),
    layers.Dense(8, activation='relu'),
    layers.Dense(1)
])

dnn_cell_model.compile(loss='mse',
            optimizer=tf.keras.optimizers.Adam(0.001))

dnn_cell_model.summary()
history = dnn_cell_model.fit(
    train_features, train_labels,
    validation_split=0.2,
    verbose=0, epochs=400) 
predicted_features = dnn_cell_model.predict(test_features[:1200])
loss_mae = np.sum(abs(np.transpose(predicted_features) - np.array(test_labels[:1200])))/len(predicted_features)
print('Loss-mean_absolute_error:',loss_mae)
