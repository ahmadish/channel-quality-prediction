from pathlib import Path
import json
import numpy as np


data_folder_mdl = Path("data/mdl/")

file_test_mdlJson = data_folder_mdl /"model.json"

#read input file
fin = open(file_test_mdlJson, "rt")
data = json.load(fin )
predicted = data['predicted_features']
test = data['test_labels']

for index, row in enumerate(predicted):
    predicted[index] = list(map(float, row))


predicted_features = np.array(predicted)
test_labels = np.array(test)

loss_mae = np.sum(abs(np.transpose((predicted_features)) - np.array(test_labels)))/len(predicted_features)
print('Loss-mean_absolute_error:',loss_mae)

