from pathlib import Path
import numpy as np
import os
from numpy import random


data_folder_txt = Path("data/txt/")
data_folder_npy = Path("data/npy/")

abs_path = os.getcwd()

filename = "data5shadowint.txt" 
filenameOut = "data5shadowZero2int.txt" 

#abs_path  = abs_path[:-4]
file_in_path= abs_path / data_folder_txt /filename
file_out_path= abs_path / data_folder_txt /filenameOut
in_data = np.loadtxt( file_in_path,delimiter = ',')

zeroProb = 0.1
out_data = in_data

for lineIdx in range(0,len(in_data)):
    probRand = random.rand()
    if probRand <zeroProb:
        column = random.randint(1,in_data.shape[1])
        in_data[lineIdx,column] = 0

np.savetxt(file_out_path, in_data, delimiter=',')
