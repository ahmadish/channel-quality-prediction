#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303




def parse_input_rats(headers,dnn_mdl):
   # for measVal in range(len(data)):
    dnn_mdl.number_inputs_cell = 0
    dnn_mdl.number_inputs_wifi = 0
    for h in headers:
        if "cell" in h:
            dnn_mdl.number_inputs_cell += 1
        if "wifi" in h:
            dnn_mdl.number_inputs_wifi += 1 

    dnn_mdl.number_inputs_total = dnn_mdl.number_inputs_cell + dnn_mdl.number_inputs_wifi     
   


from pyexpat import model
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import loadtxt, outer
from pathlib import Path
import json
import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras

np.random.seed(1)
tf.random.set_seed(2)


dnn_mdl = model.Model()
dnn_mdl.number_inputs_train = 40

train_percentage = 0.8

data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
file_data_Np = data_folder_npy / "dataNp.npy"
file_data_csv = data_folder_csv / "dataFilter.csv"
file_data_header = data_folder_txt /"dataNpHeader.txt"
file_data_combo = data_folder_txt /"comboInverse.txt"

file_test_mdl = data_folder_mdl /"model.cp.ckpt"
file_test_mdlJson = data_folder_mdl /"model.json"

dataset_Np = np.load(file_data_Np)

f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 
dataset = dataset_Np.copy()


raw_dataset = pd.read_csv(file_data_csv, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)

dnn_mdl.number_inputs_wifi = dataset.shape[1]

dataset = abs(raw_dataset.copy())
parse_input_rats(headers,dnn_mdl)


for ntrained in range(dnn_mdl.number_inputs_cell,dnn_mdl.number_inputs_total):
    wifiString = 'wifi'
    nRemove = dnn_mdl.number_inputs_total-ntrained


    if dnn_mdl.number_inputs_train > dnn_mdl.number_inputs_cell:
        for wi in range(dnn_mdl.number_inputs_totalnRemove ):
            drop_string = wifiString + str(dnn_mdl.number_inputs_train+wi-dnn_mdl.number_inputs_cell) 
            dataset = dataset.drop(drop_string,axis =1 ,errors ='ignore')

#indices = np.random.permutation(dataset.shape[0])
#n_train_records = int(train_percentage *indices.shape[0])
#training_idx, test_idx = indices[:n_train_records], indices[n_train_records:]
#training, test = dataset[training_idx,:], dataset[test_idx,:]

    train_dataset = dataset.sample(frac=0.8, random_state=0)
    test_dataset = dataset.drop(train_dataset.index)
    #print(train_dataset.describe().transpose())

    train_features = train_dataset.copy()
    test_features = test_dataset.copy()

    train_labels = train_features.pop('cell0')
    test_labels = test_features.pop('cell0')

    normalizer =  tf.keras.layers.Normalization(axis=-1)
    normalizer.adapt(np.array(train_features))
    cell1 = np.array(train_features)

    cell1_normalizer = tf.keras.layers.Normalization(input_shape=[dnn_mdl.number_inputs_train-1,], axis=None)
    cell1_normalizer.adapt(cell1)



    dnn_cell_model = keras.Sequential([
        cell1_normalizer,
        layers.Dropout(0.2, input_shape = (dnn_mdl.number_inputs_train-1,)),
        layers.Dense(40, activation='relu'),
        layers.Dropout(0.3),
        layers.Dense(20, activation='relu'),
        layers.Dropout(0.2),
        layers.Dense(18, activation='relu'),
        layers.Dense(15, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(8, activation='relu'),
        layers.Dense(1)
    ])

    dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Adam(0.001))

    dnn_cell_model.summary()


    # Create a callback that saves the model's weights
    #cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_test_mdl,
                                               #     save_weights_only=True,
                                             #       verbose=1)


    history = dnn_cell_model.fit(
        train_features, train_labels,
        validation_split=0.2,
        validation_data=(test_features, test_labels),
        verbose=0, epochs=1000),
        #callbacks=[cp_callback]) 

    predicted_features = dnn_cell_model.predict(test_features)
    loss_mae = np.sum(abs(np.transpose((predicted_features)) - np.array(test_labels)))/len(predicted_features)
    print('trained %d Loss %f' % (ntrained,loss_mae))
    out = 'trained: ' + str(ntrained) + ' loss: ' + str(loss_mae) + '\n'
    #print('Loss-mean_absolute_error:',loss_mae)
    #dnn_mdl.predicted_features = predicted_features.tolist()
    #dnn_mdl.test_features = test_features.values.tolist()
    #dnn_mdl.train_features = train_features.values.tolist()
   # dnn_mdl.test_labels = test_labels.tolist()
    #dnn_mdl.train_labels = train_labels.tolist()

   # jsonStr = json.dumps(dnn_mdl.__dict__)
    #open the input file in write mode
    fin = open(file_data_combo, "a")

    #overrite the input file with the resulting data
    fin.write(out)
    #close the file
    fin.close()
