#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303

def parse_input_rats(headers,dnn_mdl):
   # for measVal in range(len(data)):
    dnn_mdl.number_inputs_cell = 0
    dnn_mdl.number_inputs_wifi = 0
    for h in headers:
        if "cell" in h:
            dnn_mdl.number_inputs_cell += 1
        if "wifi" in h:
            dnn_mdl.number_inputs_wifi += 1 

    dnn_mdl.number_inputs_total = dnn_mdl.number_inputs_cell + dnn_mdl.number_inputs_wifi   


def drop_dataset_columns():
   # for measVal in range(len(data)):
    drop_string_columns =  ''

    for column in dnn_mdl.columns_drop_mobile:
        mobileString = 'cell'
        drop_string = mobileString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_mobile[len(dnn_mdl.columns_drop_mobile) -1]):
            drop_string_columns = drop_string_columns + ','


    for column in dnn_mdl.columns_drop_wifi:
        wifiString = 'wifi'
        drop_string = wifiString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_wifi[len(dnn_mdl.columns_drop_wifi) -1]):
            drop_string_columns = drop_string_columns + ','
    
    dnn_mdl.drop_string = drop_string_columns

def setup_data():
    dnn_mdl.train_dataset = dnn_mdl.dataset.sample(frac=0.8, random_state=0)
    dnn_mdl.test_dataset = dnn_mdl.dataset.drop(dnn_mdl.train_dataset.index)
    #print(train_dataset.describe().transpose())

    dnn_mdl.train_features = dnn_mdl.train_dataset.copy()
    dnn_mdl.test_features = dnn_mdl.test_dataset.copy()

    dnn_mdl.train_labels = dnn_mdl.train_features.pop('cell0')
    dnn_mdl.test_labels = dnn_mdl.test_features.pop('cell0')


def setup_model_layers():
    dnn_mdl.normalizer =  tf.keras.layers.Normalization(axis=-1)
    dnn_mdl.normalizer.adapt(np.array(dnn_mdl.train_features))
    dnn_mdl.cell1 = np.array(dnn_mdl.train_features)

    dnn_mdl.cell1_normalizer = tf.keras.layers.Normalization(input_shape=[dnn_mdl.number_inputs_total-len(dnn_mdl.columns_drop_wifi)-len(dnn_mdl.columns_drop_mobile)-1,], axis=None)
    dnn_mdl.cell1_normalizer.adapt(dnn_mdl.cell1)


    dnn_mdl.dnn_cell_model = keras.Sequential([
            dnn_mdl.cell1_normalizer,
            layers.Dense(22, activation='relu'),
            layers.Dropout(0.2),
            layers.Dense(15, activation='relu'),
            layers.Dropout(0.1),
            layers.Dense(12, activation='relu'),
            layers.Dense(8, activation='relu'),
            layers.Dense(4, activation='relu'),
            layers.Dense(1)
        ])
    dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Adam(0.001))


def normalize_input_data(data):
    return (data - 100)/10


from pyexpat import model
from re import S
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import loadtxt, outer
from pathlib import Path
import json
import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras

np.random.seed(1)
tf.random.set_seed(2)

plotComparison = True
normalize = False
dnn_mdl = model.Model()
dnn_mdl.columns_drop_wifi =[*range(0,44)]
dnn_mdl.columns_drop_mobile =[]

dnn_mdl.number_inputs_train = 53

train_percentage = 0.7

data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#file_data_Np = data_folder_npy / "dataNp.npy"
file_data_csv = data_folder_csv / "dataFilter.csv"
file_data= data_folder_txt / "dataO.txt"

file_data_header = data_folder_txt /"dataNpHeaderO.txt"
file_data_combo = data_folder_txt /"comboSelectedRemovalO.txt"
file_data_comboCompare = data_folder_txt /"comboSelectedRemovalCompareO.txt"

file_test_mdl = data_folder_mdl /"modelO.cp.ckpt"
file_test_mdlJson = data_folder_mdl /"modelO.json"

#dataset_Np = np.load(file_data_Np)

f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 
#dataset = dataset_Np.copy()
#dnn_mdl.dataset = dataset

raw_dataset = pd.read_csv(file_data, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)



if(normalize):
    dnn_mdl.dataset = normalize_input_data(abs(raw_dataset.copy()))
else:
     dnn_mdl.dataset = abs(raw_dataset.copy())

dnn_mdl.number_inputs_wifi = dnn_mdl.dataset.shape[1]
parse_input_rats(headers,dnn_mdl)
drop_dataset_columns()
setup_data()
setup_model_layers()
  
dnn_mdl.dnn_cell_model.summary()

# Create a callback that saves the model's weights
#cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_test_mdl,
                                        #     save_weights_only=True,
                                        #       verbose=1)

history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=0, epochs=1000),
    #callbacks=[cp_callback]) 

dnn_mdl.predicted_features = dnn_mdl.dnn_cell_model.predict(dnn_mdl.test_features)
loss_mae = np.sum(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels)))/len(dnn_mdl.predicted_features)
test_labels_list = np.array(dnn_mdl.test_labels)
pred_labels_list = np.squeeze(np.array(dnn_mdl.predicted_features.tolist()))
my_rho = np.corrcoef(test_labels_list, pred_labels_list)

print('removed %s Loss %f Pearson: %f' % (dnn_mdl.drop_string,loss_mae,my_rho[1][0]))
out = 'trained: ' + dnn_mdl.drop_string + ' loss: ' + str(loss_mae) + '\n'
compare = ''

for idx in range(len(dnn_mdl.predicted_features)):
    compare = compare + str(dnn_mdl.predicted_features[idx]) + ',' + str(test_labels_list[idx]) + '\n'



file_data_comboCompare
fin = open(file_data_comboCompare, "wt")

#overrite the input file with the resulting data
fin.write(compare)
#close the file
fin.close()



fin = open(file_data_combo, "a")

#overrite the input file with the resulting data
fin.write(out)
#close the file
fin.close()


if plotComparison:
    data_folder_png = Path("data/plot/png/")
    file_data_png = data_folder_png / "testPredictCompareAllCells.png"
    file_data_diff_png = data_folder_png / "testPredictCompareDiffAllCells.png"

    stringTitle = "Compare test & predicted Pearson: %.2f MAE: %.2f dB" % (my_rho[1][0], loss_mae)
    plt.figure(1)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(pred_labels_list, label='predicted')
    plt.legend() 
    plt.savefig( file_data_png, dpi=300)
    plt.show()


    plt.figure(2)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-pred_labels_list, label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png, dpi=300)
    plt.show()
