#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303
def parse_input_rats(headers,dnn_mdl):
   # for measVal in range(len(data)):
    dnn_mdl.number_inputs_cell = 0
    dnn_mdl.number_inputs_wifi = 0
    for h in headers:
        if "cell" in h:
            dnn_mdl.number_inputs_cell += 1
        if "wifi" in h:
            dnn_mdl.number_inputs_wifi += 1 

    dnn_mdl.number_inputs_total = dnn_mdl.number_inputs_cell + dnn_mdl.number_inputs_wifi   


def drop_dataset_columns():
   # for measVal in range(len(data)):
    drop_string_columns =  ''

    for column in dnn_mdl.columns_drop_mobile:
        mobileString = 'cell'
        drop_string = mobileString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_mobile[len(dnn_mdl.columns_drop_mobile) -1]):
            drop_string_columns = drop_string_columns + ','


    for column in dnn_mdl.columns_drop_wifi:
        wifiString = 'wifi'
        drop_string = wifiString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_wifi[len(dnn_mdl.columns_drop_wifi) -1]):
            drop_string_columns = drop_string_columns + ','
    
    dnn_mdl.drop_string = drop_string_columns

def setup_data():
    dnn_mdl.train_dataset = dnn_mdl.dataset.sample(frac=0.8, random_state=0)
    dnn_mdl.test_dataset = dnn_mdl.dataset.drop(dnn_mdl.train_dataset.index)
    #print(train_dataset.describe().transpose())

    dnn_mdl.train_features = dnn_mdl.train_dataset.copy()
    dnn_mdl.test_features = dnn_mdl.test_dataset.copy()

    dnn_mdl.train_labels = dnn_mdl.train_features.pop('cell0')
    dnn_mdl.test_labels = dnn_mdl.test_features.pop('cell0')


def setup_model_layers():
    dnn_mdl.normalizer =  tf.keras.layers.Normalization(axis=-1)
    dnn_mdl.normalizer.adapt(np.array(dnn_mdl.train_features))
    dnn_mdl.cell1 = np.array(dnn_mdl.train_features)

    tmp_input_shape = dnn_mdl.number_inputs_total-1 
    if dnn_mdl.number_inputs_wifi >0:
        tmp_input_shape = tmp_input_shape - len(dnn_mdl.columns_drop_wifi)
    if dnn_mdl.number_inputs_cell >0:
        tmp_input_shape = tmp_input_shape - len(dnn_mdl.columns_drop_mobile)

    dnn_mdl.cell1_normalizer = tf.keras.layers.Normalization(input_shape=[tmp_input_shape,], axis=None)
    dnn_mdl.cell1_normalizer.adapt(dnn_mdl.cell1)

    layer_cnt = 7
    nodes = [160, 80, 50, 30, 12, 4, 1]
    activation = ['relu', 'relu', 'relu', 'relu', 'relu', 'relu', 'none']
    dropout = [0, 0, 0, 0, 0, 0, 0] # do not change last value 

    export_dnn(layer_cnt, nodes, activation, dropout)

    dnn_mdl.dnn_cell_model = keras.Sequential([
            dnn_mdl.cell1_normalizer,
            layers.Dense(nodes[0], activation=activation[0], activity_regularizer=l1(0.01)),
            layers.Dropout(dropout[0]),
            layers.Dense(nodes[1], activation=activation[1]),
            layers.Dropout(dropout[1]),
            layers.Dense(nodes[2], activation=activation[2]),
            layers.Dropout(dropout[2]),
            layers.Dense(nodes[3], activation=activation[3]),
            layers.Dropout(dropout[3]),
            layers.Dense(nodes[4], activation=activation[4]),
            layers.Dropout(dropout[4]),
            layers.Dense(nodes[5], activation=activation[5]),
            layers.Dropout(dropout[5]),
            layers.Dense(nodes[6])
        ])

    dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.001),metrics=['mean_absolute_error'])


def  filterRsrp(data,rsrp_threshold):
    fin = open(data, "rt")
    data = fin.read()

    file_data= data_folder_txt / "dataOMFilter.txt"
    fout = open(file_data, "wt")

    lines = data.split('\n')
    for line in lines:
        if line:
            if not "cell" in line:
                rsrp_val = line.split(',')[0]
                if not int(rsrp_val)<rsrp_threshold:
                    fout.write(line+'\n')

    fout.close()
    fin.close()

def normalize_input_data(data):
    return (data - 100)/10


#from pyexpat import model
from re import S
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import empty, loadtxt, outer
from pathlib import Path
import json
from utils.dnnToJSON import export_dnn
#import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from keras.regularizers import l1
from keras.regularizers import l2
from keras.models import save_model



fixed_seed = True
if fixed_seed:
    np.random.seed(1)
    tf.random.set_seed(2)

rsrp_threshold = -110
plotComparison = True
plotHistory = True
normalize = False
dnn_mdl = tf.keras.Model()
dnn_mdl._name = 'base'
dnn_mdl.columns_drop_wifi =[*range(0,44)]
dnn_mdl.columns_drop_mobile = [] #[*range(6,38)]

dnn_mdl.number_inputs_train = 53

train_percentage = 0.7

data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#file_data_Np = data_folder_npy / "data2intNorm.npy"
file_data_csv = data_folder_csv / "dataFilter.csv"
file_data= data_folder_txt / "dataOMF.txt"
filename = "data5shadowint.txt"#"data5shadowZero2int.txt" 
filename_out_append = 'd5_shadowZero22'
file_dataFilter= data_folder_txt / filename # "dataOMFilter2.txt"
file_dataFilterAugment= data_folder_txt / "dataOMFilterAugment2.txt"

file_data_header = data_folder_txt / "data5shadowint_header.txt" #"dataNpHeaderO2.txt"
file_data_combo = data_folder_txt /"comboSelectedRemovalOM.txt"
file_data_comboCompare = data_folder_txt /"comboSelectedRemovalCompareOM.txt"

file_test_mdl = data_folder_mdl /"modelOM.cp.ckpt"
file_test_mdlJson = data_folder_mdl /"modelOM.json"

file_save_model = data_folder_mdl /"mdlShadowZerox2.tf"
#dataset_Np = np.load(file_data_Np)

f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 
#dataset = dataset_Np.copy()
#dnn_mdl.dataset = dataset
#filterRsrp(file_data,rsrp_threshold)

raw_dataset = pd.read_csv(file_dataFilter, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)


if(normalize):
    dnn_mdl.dataset = normalize_input_data(abs(raw_dataset.copy()))
else:
    dnn_mdl.dataset = abs(raw_dataset.copy())

dnn_mdl.number_inputs_wifi = dnn_mdl.dataset.shape[1]
parse_input_rats(headers,dnn_mdl)
drop_dataset_columns()
setup_data()
setup_model_layers()
  
dnn_mdl.dnn_cell_model.summary()

# Create a callback that saves the model's weights
#cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_test_mdl,
                                        #     save_weights_only=True,
                                        #       verbose=1)

history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=1, epochs=100),
    #callbacks=[cp_callback]) 

dnn_mdl.dnn_cell_model.save(file_save_model)
# Plot history (also known as a loss curve)
pd.DataFrame(history[0].history).plot()
plt.ylabel("loss")
plt.xlabel("epochs")

dnn_mdl.predicted_features = dnn_mdl.dnn_cell_model.predict(dnn_mdl.test_features)
loss_mae = np.sum(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels)))/len(dnn_mdl.predicted_features)
loss_mae_round = np.sum(np.round(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels))))/len(dnn_mdl.predicted_features)

test_labels_list = np.array(dnn_mdl.test_labels)
pred_labels_list = np.squeeze(np.array(dnn_mdl.predicted_features.tolist()))
my_rho = np.corrcoef(test_labels_list, pred_labels_list)

print('removed %s Loss %f (rounded loss %f) Pearson: %f' % (dnn_mdl.drop_string,loss_mae,loss_mae_round,my_rho[1][0]))
out = 'trained: ' + dnn_mdl.drop_string + ' loss: ' + str(loss_mae) + '\n'
compare = ''

for idx in range(len(dnn_mdl.predicted_features)):
    compare = compare + str(dnn_mdl.predicted_features[idx]) + ',' + str(test_labels_list[idx]) + '\n'



file_data_comboCompare
fin = open(file_data_comboCompare, "wt")

#overrite the input file with the resulting data
fin.write(compare)
#close the file
fin.close()



fin = open(file_data_combo, "a")

#overrite the input file with the resulting data
fin.write(out)
#close the file
fin.close()


if plotComparison:
    data_folder_png = Path("data/plot/png/")
    data_png = filename[0:len(filename)-4] + "_testPredictCompareAllCells_" +filename_out_append + ".png"
    file_data_png = data_folder_png / data_png
    data_err = filename[0:len(filename)-4] + "_testPredictCompareAllCellsErr_" +filename_out_append + "..png"
    file_data_png_err = data_folder_png / data_err

    data_png_rounded = filename[0:len(filename)-4] + "_testPredictCompareAllCellsRounded_" +filename_out_append + "..png"
    file_data_png_rounded = data_folder_png / data_png_rounded

    data_diff = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCells_" +filename_out_append + "..png"
    file_data_diff_png = data_folder_png / data_diff
    data_diff_rounded = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCellsRounded_" +filename_out_append + "..png"
    file_data_diff_png_rounded = data_folder_png / data_diff_rounded

    stringTitle = "Pearson: %.2f MAE: %.2f dB (MAE rounded: %.2f dB" % (my_rho[1][0], loss_mae,loss_mae_round)
    plt.figure(1)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(pred_labels_list, label='predicted')
    plt.legend() 
    plt.savefig( file_data_png, dpi=300)
    plt.show()


    plt.figure(2)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-pred_labels_list, label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png, dpi=300)
    plt.show()


    plt.figure(3)
    sns.distplot((test_labels_list-pred_labels_list), hist=True, kde=True, 
             bins=30, color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 4})
    plt.xlabel("error [dB]") 
    plt.ylabel("Density [-]") 
    plt.legend() 
    plt.savefig( file_data_png_err, dpi=300)
    plt.show()


if plotHistory:
  #  plt.figure(4) 
  #  plt.plot(history[0].history['accuracy'])
   # plt.plot(history[0].history['val_accuracy'])
   # plt.title('model accuracy')
    #plt.ylabel('accuracy')
    #plt.xlabel('epoch')
    #plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    # summarize history for loss
    plt.figure(5) 
    plt.yscale("log") 
    plt.plot(history[0].history['loss'])
    plt.plot(history[0].history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
if False:
    plt.figure(3)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("rounded RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(np.round(pred_labels_list), label='predicted')
    plt.legend() 
    plt.savefig( file_data_png_rounded, dpi=300)
    plt.show()

    
    plt.figure(4)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-np.round(pred_labels_list), label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png_rounded, dpi=300)
    plt.show()
