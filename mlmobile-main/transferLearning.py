#relu: for 100 epochs loss is ~1.79788; for 200 epochs loss is ~1.19405; for 300 epochs loss is ~1.35318; for 400 epochs loss is 0.89578; for 500 epochs loss is 0.97303

def parse_input_rats(headers,dnn_mdl):
   # for measVal in range(len(data)):
    dnn_mdl.number_inputs_cell = 0
    dnn_mdl.number_inputs_wifi = 0
    for h in headers:
        if "cell" in h:
            dnn_mdl.number_inputs_cell += 1
        if "wifi" in h:
            dnn_mdl.number_inputs_wifi += 1 

    dnn_mdl.number_inputs_total = dnn_mdl.number_inputs_cell + dnn_mdl.number_inputs_wifi   


def drop_dataset_columns():
   # for measVal in range(len(data)):
    drop_string_columns =  ''

    for column in dnn_mdl.columns_drop_mobile:
        mobileString = 'cell'
        drop_string = mobileString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_mobile[len(dnn_mdl.columns_drop_mobile) -1]):
            drop_string_columns = drop_string_columns + ','


    for column in dnn_mdl.columns_drop_wifi:
        wifiString = 'wifi'
        drop_string = wifiString + str(column) 
        dnn_mdl.dataset = dnn_mdl.dataset.drop(drop_string,axis =1 ,errors ='ignore')
        drop_string_columns = drop_string_columns + str(column)
        if(column != dnn_mdl.columns_drop_wifi[len(dnn_mdl.columns_drop_wifi) -1]):
            drop_string_columns = drop_string_columns + ','
    
    dnn_mdl.drop_string = drop_string_columns

def setup_data():
    dnn_mdl.train_dataset = dnn_mdl.dataset.sample(frac=0.8, random_state=0)
    dnn_mdl.test_dataset = dnn_mdl.dataset.drop(dnn_mdl.train_dataset.index)
    #print(train_dataset.describe().transpose())

    dnn_mdl.train_features = dnn_mdl.train_dataset.copy()
    dnn_mdl.test_features = dnn_mdl.test_dataset.copy()

    dnn_mdl.train_labels = dnn_mdl.train_features.pop('cell0')
    dnn_mdl.test_labels = dnn_mdl.test_features.pop('cell0')


def setup_model_layers():
    dnn_mdl.normalizer =  tf.keras.layers.Normalization(axis=-1)
    dnn_mdl.normalizer.adapt(np.array(dnn_mdl.train_features))
    dnn_mdl.cell1 = np.array(dnn_mdl.train_features)

    tmp_input_shape = dnn_mdl.number_inputs_total-1 
    if dnn_mdl.number_inputs_wifi >0:
        tmp_input_shape = tmp_input_shape - len(dnn_mdl.columns_drop_wifi)
    if dnn_mdl.number_inputs_cell >0:
        tmp_input_shape = tmp_input_shape - len(dnn_mdl.columns_drop_mobile)

    dnn_mdl.cell1_normalizer = tf.keras.layers.Normalization(input_shape=[tmp_input_shape,], axis=None)
    dnn_mdl.cell1_normalizer.adapt(dnn_mdl.cell1)


    dnn_mdl.dnn_cell_model = keras.Sequential([
            dnn_mdl.cell1_normalizer,
            #layers.Dense(12, activation='relu', activity_regularizer=l1(0.1)),
            #layers.Dense(24, activation='relu', activity_regularizer=l1(0.5)),
           # layers.Dense(200, activation='tanh', activity_regularizer=l1(0.2)),
            layers.Dense(160, activation='tanh', activity_regularizer=l1(0.4)),
            layers.Dense(80, activation='tanh'),
            #layers.Dropout(0.1),
            layers.Dense(50, activation='tanh'),
            #layers.Dropout(0.1),
            layers.Dense(30, activation='tanh'),
            #layers.Dropout(0.1),
            layers.Dense(12, activation='tanh'),
            layers.Dense(4, activation='relu'),
            layers.Dense(1)
        ])
    dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.001),metrics=['accuracy'])

def  filterRsrp(data,rsrp_threshold):
    fin = open(data, "rt")
    data = fin.read()

    file_data= data_folder_txt / "dataOMFilter.txt"
    fout = open(file_data, "wt")

    lines = data.split('\n')
    for line in lines:
        if line:
            if not "cell" in line:
                rsrp_val = line.split(',')[0]
                if not int(rsrp_val)<rsrp_threshold:
                    fout.write(line+'\n')

    fout.close()
    fin.close()

def normalize_input_data(data):
    return (data - 100)/10


#from pyexpat import model
from re import S
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import empty, loadtxt, outer
from pathlib import Path
import json
import utils.Model as model

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from keras.regularizers import l1
from keras.regularizers import l2
from utils.dnnToJSON import export_dnn, export_dnn_results
#from keras.models import save



fixed_seed = True
if fixed_seed:
    np.random.seed(1)
    tf.random.set_seed(2)

rsrp_threshold = -110
plotComparison = True
plotHistory = False
normalize = False
dnn_mdl = model.Model()
dnn_mdl.columns_drop_wifi =[] #[*range(0,44)]
dnn_mdl.columns_drop_mobile = [] # [*range(6,39)]

dnn_mdl.number_inputs_train = 53

train_percentage = 0.7

data_folder_npy = Path("data/npy/")
data_folder_txt = Path("data/txt/")
data_folder_csv = Path("data/csv/")
data_folder_mdl = Path("data/mdl/")

# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#file_data_Np = data_folder_npy / "dataNp.npy"
file_data_csv = data_folder_csv / "dataFilter.csv"
file_data= data_folder_txt / "dataOMF.txt"
filename = "data1int.txt" 
filename = "dmod1.txt" 

filename_out_append = 'tl5zero22'
file_dataFilter= data_folder_txt / "dataOFilterAugment.txt"
file_dataFilterAugment= data_folder_txt / "dataOMFilterAugment2.txt"

if len(sys.argv) > 1:
    file_datamod=data_folder_txt / sys.argv[1] # if specified by input
else:
    file_datamod= data_folder_txt / "data5shadowint_aug.txt" # default

file_data_header = data_folder_txt / "dataMeas2FilterAugmentCell5_header.txt"
file_data_combo = data_folder_txt /"comboSelectedRemovalOM.txt"
file_data_comboCompare = data_folder_txt /"comboSelectedRemovalCompareOM.txt"

file_test_mdl = data_folder_mdl /"modelOM.cp.ckpt"
file_test_mdlJson = data_folder_mdl /"modelOM.json"

file_save_model = data_folder_mdl /"mdlShadowZerox2.tf"
#dataset_Np = np.load(file_data_Np)

f = open(file_data_header)
data_Np_Header = f.read()
headers = data_Np_Header.split(';')
headers.pop() 
#dataset = dataset_Np.copy()
#dnn_mdl.dataset = dataset
#filterRsrp(file_data,rsrp_threshold)

raw_dataset = pd.read_csv(file_datamod, names=headers,
                        na_values='?', comment='\t',
                        sep=',', skipinitialspace=False)


if(normalize):
    dnn_mdl.dataset = normalize_input_data(abs(raw_dataset.copy()))
else:
    dnn_mdl.dataset = abs(raw_dataset.copy())

dnn_mdl.number_inputs_wifi = dnn_mdl.dataset.shape[1]
parse_input_rats(headers,dnn_mdl)
drop_dataset_columns()
setup_data()
#setup_model_layers()
  
load_mdl  = tf.keras.models.load_model(file_save_model)
load_mdl.pop()
load_mdl.pop()
load_mdl._name = 'base'
for i in range(4):
    load_mdl.layers[i].trainable = False

for i in range(4,5):
    load_mdl.layers[i].trainable = True

ll = load_mdl.layers[5].output
ll = layers.Dense(20,name='topOut2')(ll)
ll = layers.Dense(4,name='topOut1')(ll)
ll = layers.Dense(1,name='topOut0')(ll)
ll._name = 'top'
dnn_mdl.dnn_cell_model = tf.keras.Model(inputs=load_mdl.input,outputs=ll)

#dnn_mdl.dnn_cell_model.trainable = False

dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.001),metrics=['mean_absolute_error'])

# Create a callback that saves the model's weights
#cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_test_mdl,
                                        #     save_weights_only=True,
                                        #       verbose=1)
history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=1, epochs=20),
    #callbacks=[cp_callback]) 

for i in range(4):
    load_mdl.layers[i].trainable = True # unfreeze reused layers

dnn_mdl.dnn_cell_model.compile(loss='mean_squared_error',
                optimizer=tf.keras.optimizers.Nadam(0.0001),metrics=['mean_absolute_error'])

history = dnn_mdl.dnn_cell_model.fit(
    dnn_mdl.train_features, dnn_mdl.train_labels,
    validation_split=0.2,
    validation_data=(dnn_mdl.test_features, dnn_mdl.test_labels),
    verbose=1, epochs=180),
    #callbacks=[cp_callback])                 

#dnn_mdl.dnn_cell_model.save(file_save_model)
# Plot history (also known as a loss curve)
pd.DataFrame(history[0].history).plot()
plt.ylabel("loss")
plt.xlabel("epochs");

dnn_mdl.predicted_features = dnn_mdl.dnn_cell_model.predict(dnn_mdl.test_features)
loss_mae = np.sum(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels)))/len(dnn_mdl.predicted_features)
loss_mae_round = np.sum(np.round(abs(np.transpose((dnn_mdl.predicted_features)) - np.array(dnn_mdl.test_labels))))/len(dnn_mdl.predicted_features)

test_labels_list = np.array(dnn_mdl.test_labels)
pred_labels_list = np.squeeze(np.array(dnn_mdl.predicted_features.tolist()))
my_rho = np.corrcoef(test_labels_list, pred_labels_list)

export_dnn_results(loss_mae, loss_mae_round, my_rho[1][0])

print('removed %s Loss %f (rounded loss %f) Pearson: %f' % (dnn_mdl.drop_string,loss_mae,loss_mae_round,my_rho[1][0]))
out = 'trained: ' + dnn_mdl.drop_string + ' loss: ' + str(loss_mae) + '\n'
compare = ''

for idx in range(len(dnn_mdl.predicted_features)):
    compare = compare + str(dnn_mdl.predicted_features[idx]) + ',' + str(test_labels_list[idx]) + '\n'

file_data_comboCompare
fin = open(file_data_comboCompare, "wt")

#overrite the input file with the resulting data
fin.write(compare)
#close the file
fin.close()

fin = open(file_data_combo, "a")

#overrite the input file with the resulting data
fin.write(out)
#close the file
fin.close()

if plotComparison:
    data_folder_png = Path("data/plot/png/")
    data_png = filename[0:len(filename)-4] + "_testPredictCompareAllCells_" +filename_out_append + ".png"
    file_data_png = data_folder_png / data_png
    data_err = filename[0:len(filename)-4] + "_testPredictCompareAllCellsErr_" +filename_out_append + "..png"
    file_data_png_err = data_folder_png / data_err

    data_png_rounded = filename[0:len(filename)-4] + "_testPredictCompareAllCellsRounded_" +filename_out_append + "..png"
    file_data_png_rounded = data_folder_png / data_png_rounded

    data_diff = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCells_" +filename_out_append + "..png"
    file_data_diff_png = data_folder_png / data_diff
    data_diff_rounded = filename[0:len(filename)-4] + "_testPredictCompareDiffAllCellsRounded_" +filename_out_append + "..png"
    file_data_diff_png_rounded = data_folder_png / data_diff_rounded

    stringTitle = "Pearson: %.2f MAE: %.2f dB (MAE rounded: %.2f dB" % (my_rho[1][0], loss_mae,loss_mae_round)
    plt.figure(1)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(pred_labels_list, label='predicted')
    plt.legend() 
    plt.savefig( file_data_png, dpi=300)
    plt.show()


    plt.figure(2)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-pred_labels_list, label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png, dpi=300)
    plt.show()


    plt.figure(3)
    sns.histplot((test_labels_list-pred_labels_list), kde=True, stat= 'probability',
             bins=30, color = 'darkblue')
    #sns.displot((test_labels_list-pred_labels_list), kde=True, 
    #         bins=30, color = 'darkblue')
    plt.xlabel("error [dB]") 
    plt.ylabel("Density [-]") 
    plt.legend() 
    plt.savefig( file_data_png_err, dpi=300)
    plt.show()


if plotHistory:
    plt.figure(4) 
    plt.plot(history[0].history['accuracy'])
    plt.plot(history[0].history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.figure(5) 
    plt.yscale("log") 
    plt.plot(history[0].history['loss'])
    plt.plot(history[0].history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
if False:
    plt.figure(3)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("rounded RSRP [dBm]") 
    plt.plot(test_labels_list, label='test')
    plt.plot(np.round(pred_labels_list), label='predicted')
    plt.legend() 
    plt.savefig( file_data_png_rounded, dpi=300)
    plt.show()

    
    plt.figure(4)    
    plt.title(stringTitle) 
    plt.xlabel("sample/time") 
    plt.ylabel("RSRP [dBm]") 
    plt.plot(test_labels_list-np.round(pred_labels_list), label='test')
    plt.legend() 
    plt.savefig( file_data_diff_png_rounded, dpi=300)
    plt.show()
