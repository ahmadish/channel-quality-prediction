class MeasurementStrings(object):
    def __init__(self):
        self.string_empty_meas = '":[]}'
        self.string_empty_meas_start = '{"m'
        self.string_empty_meas_start_empty = ']\t\n'
        self.string_file_start = '{\n"measurements":\n \t {\n\t\t'
        self.string_file_end = '\n \t}\n}'
