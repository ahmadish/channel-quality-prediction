import numpy as no
import tensorflow as tf
class Model(object):
    def __init__(self):
        self.dnn_cell_model = None
        self.predicted_features = None
        self.test_features = None
        self.test_labels = None
        self.train_features = None
        self.train_labels = None
        self.number_inputs_total = None
        self.number_inputs_cell = None
        self.number_inputs_wifi = None
        self.number_inputs_train = None
        self.dataset = None
        self.normalizer = None
        self.cell1 = None
        self.cell1_normalizer = None
        self.columns_drop_wifi = None
        self.columns_drop_mobile = None
    