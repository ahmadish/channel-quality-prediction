def get_commit_hash():
    try:
        commit = subprocess.check_output(
            ['git', 'rev-parse', '--short', 'HEAD']).decode().strip()
    except subprocess.CalledProcessError:
        commit = "0000000"
    return commit

def export_dnn(layers, nodes, act, drops):
    file = Path('data/dnn_json/dnn_tmp.json')
    dt = datetime.now()
    ts = datetime.timestamp(dt) # get timestamp
    hash = get_commit_hash()
    jsonObj = '{"General":[{"layer_cnt":'+'"'+str(layers)+'","time_stamp":'+'"'+str(ts)+'","commit_hash":'+'"'+str(hash)+'"}],'
    
    for layer in range(0, layers):
         node_str = '"node_cnt":' + '"' + str(nodes[layer]) + '",'
         act_str  = '"activation":' + '"' + str(act[layer]) + '",'
         drop_str = '"dropout_after":' + '"' + str(drops[layer]) + '"'
         layerObj = '"Layer'+str(layer+1)+'":[{'+ node_str + act_str + drop_str + '}],'
         #if not layer == (layers-1):
         #    layerObj += ','
         jsonObj += layerObj
    #jsonObj += '}\n'

    fp = open(file, "a")
    fp.write(jsonObj)
    fp.close()

def export_dnn_results(mae, rounded_mae, pearsons):
    file_in = Path('data/dnn_json/dnn_tmp.json')
    file_out = Path('data/dnn_json/dnn.json')

    fp = open(file_in, 'r')
    data = fp.read()
    fp.close()
    os.remove(file_in)

    perf_json = '"Performance":[{"MAE":'+'"'+str(mae)+'",'+'"MAE_rounded":'+'"'+str(rounded_mae)+'",'+'"Pearson\'s":'+'"'+str(pearsons)+'"}]}\n'
    jsonObj = data + perf_json
    fp = open(file_out, 'a')
    fp.write(jsonObj)
    fp.close()

import subprocess
import json
import os
from datetime import datetime
from pathlib import Path
