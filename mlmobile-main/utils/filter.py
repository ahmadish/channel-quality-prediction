#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from scipy.signal import butter,filtfilt# Filter requirements.

def replacePeak(diff,data):
    diffNp = np.array(diff)
    dataNp = np.array(data)

    for i in range(len(diffNp)):
        if (diffNp[i] != 0):
            idx = np.where(diffNp[i+1:]==-diffNp[i])
            for dist in idx[0]:
                    if(dist!=-1):
                        if(dist)<=timeDiff:
                            dataNp[i+1:i+dist+2] = dataNp[i]
    return dataNp


def timeSeriesFilter(data,tDiffSamples,valDiff):
   # for measVal in range(len(data)):

    delta = data-np.roll(data,-1)
    data = replacePeak(delta,data)
    
    return data



def filter(data,tDiffSamples,valDiff):

  return  timeSeriesFilter(data,tDiffSamples,valDiff)

 
def butter_lowpass_filter(data):

    # Filter requirements.
    T = 5.0         # Sample Period
    fs = 300e3       # sample rate, Hz
    cutoff = 100e3      # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hznyq = 0.5 * fs  # Nyquist Frequencyorder = 2       # sin wave can be approx represented as quadratic
    n = int(T * fs) # total number of samples
    nyq = 0.5 * fs  # Nyquist Frequencyorder = 2       # sin wave can be approx represented as quadratic
    order = 2       # sin wave can be approx represented as quadratic

    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y
     



timeDiff = 4
valDiff = 10
a = np.array([2, 3, 4])
data_folder_npy = Path("data/npy/")
data_folder_svg = Path("data/plot/svg/")
data_folder_png = Path("data/plot/png/")
data_folder_csv= Path("data/csv/")

file_data_svg = data_folder_svg / "data.svg"
file_filter_svg = data_folder_svg / "filter.svg"

file_data_png = data_folder_png / "data.png"
file_filter_png = data_folder_png / "filter.png"
file_pfilter_png = data_folder_png / "pandaFilter.png"

file_dataNp = data_folder_npy / "dataNp.npy"

file_datafilter = data_folder_npy / "dataFilter"

file_datafilter_csv = data_folder_csv/ "dataFilter.csv"

dataArray = np.load(file_dataNp)
dataArrayCopy = np.copy(dataArray)

dataArrayCopy = np.roll(dataArrayCopy,-1,axis=0)

diff = dataArray - dataArrayCopy

plt.figure(3)    
plt.title("Matplotlib demo") 
plt.xlabel("sample/time") 
plt.ylabel("RSRP [dBm]") 
plt.plot(dataArray) 
#plt.show()
plt.savefig(file_data_png)

#pData = pd.DataFrame(dataArray)
#pDataFilter = pData.rolling(window =10).mean()


dataArrayCopy = np.copy(dataArray)
size = dataArray.shape[1]
for rat in range(size):
    data = dataArrayCopy[:,rat]
    filteredData = filter(data,timeDiff,valDiff)
    dataArrayCopy[:,rat] = filteredData

#plt.figure(2)    
plt.title("Matplotlib demo") 
plt.xlabel("sample/time") 
plt.ylabel("RSRP [dBm]") 
plt.plot(dataArrayCopy) 
#plt.show()
plt.savefig( file_filter_png)
#print(dataArrayCopy)

np.save(file_datafilter,dataArrayCopy)
np.savetxt(file_datafilter_csv, dataArrayCopy, delimiter=",")
