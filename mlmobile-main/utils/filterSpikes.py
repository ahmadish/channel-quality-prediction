from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from scipy.signal import butter,filtfilt# Filter requirements.


def main():
  timeDiff = 2
  valDiff = 5
  a = np.array([2, 3, 4])
  data_folder_npy = Path("data/npy/")
  data_folder_svg = Path("data/plot/svg/")
  data_folder_png = Path("data/plot/png/")

  file_data_svg = data_folder_svg / "data.svg"
  file_filter_svg = data_folder_svg / "filter.svg"

  file_data_png = data_folder_png / "data.png"
  file_filter_png = data_folder_png / "filter.png"
  file_pfilter_png = data_folder_png / "pandaFilter.png"

  file_dataNp = data_folder_npy / "dataNp.npy"

  file_datafilter = data_folder_npy / "dataFilter"

  dataArray = np.load(file_dataNp)
  dataArrayCopy = np.copy(dataArray)

  dataArrayCopy = np.roll(dataArrayCopy,-1,axis=0)

  diff = dataArray - dataArrayCopy


  #pData = pd.DataFrame(dataArray)
  #pDataFilter = pData.rolling(window =10).mean()




  dataArrayCopy = np.copy(dataArray)
  size = dataArray.shape[1]
  for rat in range(size):
      data = dataArrayCopy[:,rat]
      filteredData = filterData(data)

      #plt.figure(2)    
      plt.title("Matplotlib demo") 
      plt.xlabel("sample/time") 
      plt.ylabel("RSRP [dBm]") 
      plt.plot(filteredData) 
      #plt.show()
      plt.savefig("rat_" + str(rat) + "_" + file_filter_png)
      #print(dataArrayCopy)


if __name__ == "__main__":
   main()  


def filterData(self,data):

  return  butter_lowpass_filter(self,data)

 
def butter_lowpass_filter(self,data):

    # Filter requirements.
    T = 5.0         # Sample Period
    fs = 30.0       # sample rate, Hz
    cutoff = 2      # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hznyq = 0.5 * fs  # Nyquist Frequencyorder = 2       # sin wave can be approx represented as quadratic
    n = int(T * fs) # total number of samples
    nyq = 0.5 * fs  # Nyquist Frequencyorder = 2       # sin wave can be approx represented as quadratic
    order = 2       # sin wave can be approx represented as quadratic

    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y