import json
from pathlib import Path
data_folder_meas = Path("data/meas/")
file_to_open = data_folder_meas / "mW.measWifi"

data_folder_json = Path("data/sjson/")
file_to_json = data_folder_meas / "mWt.json"


f = open('mW.json')
sampleIndex = 0
fin = open(file_to_open, "rt")
dataTime = fin.read()
dataTime = dataTime.split("\n")
data = json.load(f)
meas = data['measurements']
for m in meas:
    for rec in meas[m]:
       # print("%s: %f" % (rec['SSID'], rec['TIME']))
        rec['TIME'] = int(dataTime[sampleIndex])
        sampleIndex+=1
        #print("%s: %f" % (rec['SSID'], rec['TIME']))

f.close()

#open the input file in write mode
fin = open(file_to_json, "wt")
#overrite the input file with the resulting data
fin.write(json.dumps(data, indent=4))
#close the file
fin.close()