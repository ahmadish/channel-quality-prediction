from pathlib import Path
import json

data_folder_txt = Path("data/txt/")
data_folder_json = Path("data/json/")

file_mWTimes= data_folder_txt / "mWtimes.txt"
file_mMtimes = data_folder_txt / "mMtimes.txt"
file_fWN = data_folder_json / "wifiInput.json"
file_fMN = data_folder_json / "cellInput.json"


fMN = open(file_fMN)
fWN = open(file_fWN)
dataMN = json.load(fMN)
dataWN = json.load(fWN)

measW = dataWN['measurements']
outTimesW = ''
for m in measW:
    avgTime = 0
    numberOfSamples = 0
    for rec in measW[m]:
       # print("%s: %f" % (rec['SSID'], rec['TIME']))
        avgTime += int(rec['TIME'])
        #print("%s: %f" % (rec['SSID'], rec['TIME']))
        numberOfSamples+=1

    avgTime = int(avgTime/numberOfSamples)
    outTimesW = outTimesW + m + ':' + str(avgTime) + '\n'

measM = dataMN['measurements']

outTimesM = ''
for m in measM:
    avgTime = 0
    numberOfSamples = 0
    for rec in measM[m]:
       # print("%s: %f" % (rec['SSID'], rec['TIME']))
        avgTime += int(rec['time'])
        #print("%s: %f" % (rec['SSID'], rec['TIME']))
        numberOfSamples+=1

    avgTime = int(avgTime/numberOfSamples)
    outTimesM = outTimesM + m + ':' + str(avgTime) + '\n'    


fMN.close()
fWN.close()

#open the input file in write mode
fin = open(file_mWTimes, "wt")
#overrite the input file with the resulting data
fin.write(outTimesW)
#close the file
fin.close()

#open the input file in write mode
fin = open(file_mMtimes, "wt")
#overrite the input file with the resulting data
fin.write(outTimesM)
#close the file
fin.close()
