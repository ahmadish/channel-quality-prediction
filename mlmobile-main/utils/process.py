from pathlib import Path
import re
from itertools import count

data_folder_json = Path("data/json/")
file_ff = data_folder_json / "ff.json"

data_folder_raw = Path("data/raw/")

file_ff = data_folder_raw / "qm1meas.json"

nofix = True
mIdx = 0
counter = count(mIdx)   

#read input file
fin = open(file_ff, "rt")
#read file contents to string
data = fin.read()
if(data.__contains__('measurements')):
    nofix = False

if(nofix):
    data = '{"measurements":\n    {\t\n        "m@":\t\n' + data
    data = data.replace(',[]','')
    data = data.replace('"}],[{"tec','"}\t\n        ],\t\n        "m@":\t\n        [\t\n            {"tec')
    data = data.replace('}]}', '}\t\n        ]\t\n    }\t\n}')
    data = data.replace('[{"technology"','        [\t\n            {"technology"')
    data = data.replace('"},{"technology"','"},\t\n            {"technology"')
    data = data + '\t\n    }\t\n}'
    data = re.sub(r'@', lambda x: str(next(counter)), data )
#replace all occurrences of the required string
#data = data.replace('"},{"tech', '"},\t\n            {"tech')
#data = data.replace('":[{"tech', '": \t\n        [\t\n            {"tech')

#data = data.replace('}]},{"m', '}\t\n        ],\t\n    "m')
#data = data.replace(']},}', '\t\n        ]\t\n    }\t\n}')
#data = data.replace('{"m0"','{\t\n    "m0"')

#close the input file
fin.close()

#open the input file in write mode
fin = open(file_ff, "wt")
#overrite the input file with the resulting data
fin.write(data)
#close the file
fin.close()



