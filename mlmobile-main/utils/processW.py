from pathlib import Path
import re
from itertools import count

nofix = True
mIdx = 0
counter = count(mIdx)   
data_folder_json = Path("data/json/")
file_measureAllWifi = data_folder_json / "measureAllWifi.json"
file_mWjson = data_folder_json / "mW.json"


data_folder_raw= Path("data/raw/")
file_measureAllWifi = data_folder_raw / "qm1measWifi.json"
file_mWjson = data_folder_raw / "qm1measWifi.json"


#read input file
fin = open(file_measureAllWifi, "rt")
#read file contents to string
data = fin.read()
if(data.__contains__('measurements')):
    nofix = False

if(nofix):
    data = '{"measurements":\n    {\t\n        "m@":\t\n' + data
    data = data.replace(',[]','')
    data = data.replace('}],[{"SSID','}\t\n        ],\t\n        "m@":\t\n        [\t\n            {"SSID')
    data = data.replace('}]}', '}\t\n        ]\t\n    }\t\n}')
    data = data.replace('[{"SSID"','        [\t\n            {"SSID"')
    data = data.replace('},{"SSID"','},\t\n            {"SSID"')
    data = data + '\t\n    }\t\n}'
    data = re.sub(r'@', lambda x: str(next(counter)), data )
    
#replace all occurrences of the required string
#data = data.replace('"},{"tech', '"},\t\n            {"tech')
#data = data.replace('":[{"tech', '": \t\n        [\t\n            {"tech')

#data = data.replace('}]},{"m', '}\t\n        ],\t\n    "m')
#data = data.replace(']},}', '\t\n        ]\t\n    }\t\n}')
#data = data.replace('{"m0"','{\t\n    "m0"')

#close the input file
fin.close()

#open the input file in write mode
fin = open(file_mWjson, "wt")
#overrite the input file with the resulting data
fin.write(data)
#close the file
fin.close()



