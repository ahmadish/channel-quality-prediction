from pathlib import Path
from enum import unique
import json


data_folder_txt = Path("data/txt/")
data_folder_json = Path("data/json/")
data_folder_raw = Path("data/raw/")

file_fMids = data_folder_txt / "uniqueMIds.txt"
file_fWids = data_folder_txt / "uniqueWIds.txt"
file_fW = data_folder_json / "wifiInput.json"
file_fM = data_folder_json / "cellInput.json"


file_fW = data_folder_json / "wifiInput.json"
file_fM = data_folder_json / "cellInput.json"

fMN = open(file_fM)
fWN = open(file_fW)
dataMN = json.load(fMN)
dataWN = json.load(fWN)

measW = dataWN['measurements']
measM = dataMN['measurements']

uniqueM = []
uniqueW = []
#open the input file in write mode
fin = open(file_fWids, "wt")
for m in measW:
    for rec in measW[m]:
        if not rec['BSSID'] in uniqueW:
            uniqueW.append(rec['BSSID'])
            #overrite the input file with the resulting data
            fin.write(rec['BSSID'] + '\n')
#close the file
fin.close()

#open the input file in write mode
fin = open(file_fMids, "wt")
for m in measM:
    for rec in measM[m]:
        if not rec['PCI'] in uniqueM:
            uniqueM.append(rec['PCI'])
            #overrite the input file with the resulting data
            fin.write(rec['PCI'] + '\n')
#close the file
fin.close()
