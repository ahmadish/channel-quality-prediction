def plot_loss(history):
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['val_loss'], label='val_loss')
  plt.ylim([0, 10])
  plt.xlabel('Epoch')
  plt.ylabel('Error [MPG]')
  plt.legend()
  plt.grid(True)
  plt.show()

def plot_cell(x, y):
  plt.scatter(train_features['31'], train_labels, label='Data')
  plt.plot(x, y, color='k', label='Predictions')
  plt.xlabel('31')
  plt.ylabel('11')
  plt.legend()
  plt.show()


def build_and_compile_model(norm):
  model = keras.Sequential([
      norm,
      layers.Dense(20, activation='relu'),
      layers.Dense(18, activation='relu'),
      layers.Dense(15, activation='relu'),
      layers.Dense(12, activation='relu'),
      layers.Dense(8)
  ])

  model.compile(loss='mean_absolute_error',
                optimizer=tf.keras.optimizers.Adam(0.001))
  return model


def build_and_compile_modelM():
  model = keras.Sequential([
      layers.Dense(20, activation='relu'),
      layers.Dense(18, activation='relu'),
      layers.Dense(15, activation='relu'),
      layers.Dense(12, activation='relu'),
      layers.Dense(8, activation='relu'),
      layers.Dense(1)
  ])

  model.compile(loss='mean_absolute_error',
                optimizer=tf.keras.optimizers.Adam(0.001))
  return model

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import loadtxt

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras


# Make numpy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)

dataset = loadtxt('data.txt', delimiter=',')
column_names = ['31', '32', '33', '11']
url = 'http://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data'
column_names2 = ['MPG', 'Cylinders', 'Displacement', 'Horsepower', 'Weight',
                'Acceleration', 'Model Year', 'Origin']


raw_dataset = pd.read_csv('data.txt', names=column_names,
                          na_values='?', comment='\t',
                          sep=',', skipinitialspace=True)

dataset = raw_dataset.copy()
dataset.tail()
dataset.isna().sum()

train_dataset = dataset.sample(frac=0.8, random_state=0)
test_dataset = dataset.drop(train_dataset.index)
train_dataset.describe().transpose()

train_features = train_dataset.copy()
test_features = test_dataset.copy()

train_labels = train_features.pop('11')
test_labels = test_features.pop('11')



normalizer =  tf.keras.layers.Normalization(axis=-1)
normalizer.adapt(np.array(train_features))


cell1 = np.array(train_features['31'])

cell1_normalizer = tf.keras.layers.Normalization(input_shape=[1,], axis=None)
cell1_normalizer.adapt(cell1)

cell1_model = tf.keras.Sequential([
    cell1_normalizer,
    layers.Dense(units=1)
])

cell1_model.summary()
print(cell1_model.predict(cell1[:10]))

cell1_model.compile(
    optimizer=tf.optimizers.Adam(learning_rate=0.01),
    loss='mean_absolute_error')

history = cell1_model.fit(
    train_features['31'], train_labels,
    epochs=100,
    # suppress logging
    verbose=0,
    # Calculate validation results on 20% of the training data
    validation_split = 0.2)

hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
hist.tail()


#plot_loss(history)

linear_model = tf.keras.Sequential([
    normalizer,
    layers.Dense(units=1)
])

linear_model.predict(train_features[:10])
linear_model.compile(
    optimizer=tf.optimizers.Adam(learning_rate=0.1),
    loss='mean_absolute_error')

history = linear_model.fit(
    train_features, train_labels, 
    epochs=100,
    # suppress logging
    verbose=0,
    # Calculate validation results on 20% of the training data
    validation_split = 0.2)
print(linear_model.predict(cell1[:10]))

cell1 = np.array(train_features)

dnn_cell_model = build_and_compile_modelM()

dnn_cell_model.summary()
history = dnn_cell_model.fit(
    train_features, train_labels,
    validation_split=0.2,
    verbose=0, epochs=100)

#x = tf.linspace(-100, -75, 25)
y = dnn_cell_model.predict(cell1[:10])
#plot_cell(x,y)


#plot_loss(history)


