#!/usr/bin/env python

import socket
import getopt
import sys
import time
import _thread
import struct
import tensorflow as tf
from keras import layers
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import loadtxt


def Main():
    host = '147.32.219.115'
    #host = '192.168.25.239'
    # host = '192.168.27.124'
    port = 1234

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))

    print("Server is running and awaiting connection")
    s.listen(1)
    while True:
        c, addr = s.accept()
        print("Client connected")
        # message =("Welcome client")
        # send_msg(c,message)
        checkReceived(c, host)
    # _thread.start_new_thread(checkReceived(c,host),())


def build_and_compile_model(norm):
    model = tf.keras.Sequential([
        norm,
        layers.Dense(20, activation='relu'),
        layers.Dense(18, activation='relu'),
        layers.Dense(15, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def build_and_compile_modelM(norm):
    model = tf.keras.Sequential([
        norm,
        layers.Dense(20, activation='relu'),
        layers.Dense(18, activation='relu'),
        layers.Dense(15, activation='relu'),
        layers.Dense(12, activation='relu'),
        layers.Dense(8, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def build_and_compile_modelApp(norm, layerParam):
    model = tf.keras.Sequential()

    # add layers
    for layerParamLevel in layerParam:
        layerLevel = layers.Dense(layerParamLevel, activation='relu')
        model.add(tf.keras.layers.Dense(layerLevel))

    # add output layer
    layerLevel = layers.Dense(1)
    model.add(tf.keras.layers.Dense(layerLevel))

    """
        model = keras.Sequential([
            norm,
            layers.Dense(20, activation='relu'),
            layers.Dense(18, activation='relu'),
            layers.Dense(15, activation='relu'),
            layers.Dense(12, activation='relu'),
            layers.Dense(8, activation='relu'),
            layers.Dense(1)
        ])
    """
    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def train_dnn(train_file, neighboring_cells, target_cell):
    # Make numpy printouts easier to read.
    np.set_printoptions(precision=3, suppress=True)

    # dataset = loadtxt(train_file, delimiter=',')
    column_names = [neighboring_cells, target_cell]

    raw_dataset = pd.read_csv(train_file, names=column_names,
                              na_values='?', comment='\t',
                              sep=',', skipinitialspace=True)

    dataset = raw_dataset.copy()
    dataset.tail()
    dataset.isna().sum()

    train_dataset = dataset.sample(frac=0.8, random_state=0)
    test_dataset = dataset.drop(train_dataset.index)
    train_dataset.describe().transpose()

    train_features = train_dataset.copy()
    test_features = test_dataset.copy()

    train_labels = train_features.pop(target_cell)
    test_labels = test_features.pop(target_cell)

    normalizer = tf.keras.layers.Normalization(axis=-1)
    normalizer.adapt(np.array(train_features))
    cell1 = np.array(train_features)

    cell1_normalizer = tf.keras.layers.Normalization(input_shape=[3, ], axis=None)
    cell1_normalizer.adapt(cell1)

    dnn_cell_model = build_and_compile_modelM(cell1_normalizer)

    dnn_cell_model.summary()
    history = dnn_cell_model.fit(
        train_features, train_labels,
        validation_split=0.2,
        verbose=0, epochs=100)

    # x = tf.linspace(-100, -75, 25)


def predictRsrp(x):
    y = dnn_cell_model.predict(x)
    return y


def WaitForNewConnection(s, host):
    # host = '192.168.27.111'
    # port = 1236
    # s.close
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # s.bind((host,port))
    # s.listen(1)
    c, addr = s.accept()
    print("Client connected")

    checkReceived(c, host)


def Hello(c, data, host):
    if data != 'HELLO':
        message = ("Server: 510 Sorry i can not service your request. \n Connection Closed")
        send_msg(c, message)
        # c.send(message.encode('utf-8'))
        c.close()

    message = ("Server: 210 Hello " + host + " pleased to meet you.")
    send_msg(c, message)
    # c.send(message.encode('utf-8'))
    # Main()


def BYE(c):
    message = ("Server: 600 See Ya Later!")
    send_msg(c, message)
    print("Client Disconnected")
    # WaitForNewConnection(c,host)
    # c.send(message.encode('utf-8'))


# def sendData(c):
# check for new data
# if new data send them to client

def checkReceived(c, host):
    loop = True
    lineNum = 0
    while loop:
        data = recv_msg(c)
        # data = c.recv(1024).decode('utf-8')
        if data != None:
            data = data.decode('utf-8')
            if data == 'HELLO':
                Hello(c, data, host)
            elif data == 'BYE':
                BYE(c)
                loop = False
            elif data == 'COLLECT':
                collect(c, 0, 0)
            elif data == 'COLLECT1':
                collect(c, 1, 0)
            elif data == 'COLLECTN':
                lineNum = collect(c, 2, lineNum)
            elif data == 'UPLOAD_DATA':
                upload_data()
            elif data == 'TRAIN':
                train()
            elif data == 'PREDICT':
                predict_control(data, c)
            else:
                message = ("Invalid command")
                send_msg(c, message)
                # c.send(message.encode('utf-8'))


def collect(c, nMeas, lineNum):
    import time
    import os

    startString = "Serving"

    fileIn = "/tmp/test.txt"  # sys.argv[1]
    # fileIn = "measc.txt"
    if (nMeas == 2):
        lineNumberStart = lineNum  # int(sys.argv[2])
    else:
        lineNumberStart = 0  # int(sys.argv[2])

    release = False
    if (release):
        if os.path.exists(fileIn):
            os.remove(fileIn)
        else:
            print("Can not delete the file as it doesn't exists, yet")
        print("Waiting for file to be created")

    while not os.path.exists(fileIn):
        time.sleep(10)  # Sleep for 10 seconds

    print("File has been created, staring check")

    # Verify source file
    if not os.path.isfile(fileIn):
        print("error: {} does not exist".format(fileIn))
        sys.exit(1)

    lineNumber = 0
    loop = True
    while loop:
        # Process the file line-by-line
        fpIn = open(fileIn, 'r')
        Lines = fpIn.readlines()
        if len(Lines) > lineNumberStart:

            for line in Lines:
                lineNumber += 1

                if loop and (lineNumber > lineNumberStart):
                    startStringIndex = line.find(startString)
                    startStringIndex += len(startString)
                    measurements = "Server MEAS: " + line[startStringIndex:]
                    # measurement = measurements.split(";")
                    send_msg(c, measurements)
                    if (nMeas > 0):
                        loop = False
                        break
                    # for meas in measurement:
                    # comIdx = meas.find(",",1)
                    # cellId = meas["Cell Id: ".__len__:comIdx]
                    # rsrp = meas[comIdx + " RSRP: ".__len__:]
                    # message = meas
                    # send_msg(c,meas)
            lineNumberStart = lineNumber
            fpIn.close()
        if (nMeas == 2):
            print("Send line: " + str(lineNumber))
            return lineNumber


def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg.encode('UTF-8')
    sock.sendall(msg)


def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Read the message data
    return recvall(sock, msglen)


def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = bytearray()
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data.extend(packet)
    return data


def train():
    # PARSE INPUT file - if local collection then get "/tmp/test.txt"
    # neighboring_cells
    # target_cell

    neighboring_cells = ['31', '32', '33']
    target_cell = ['11']
    train_dnn(train_file, neighboring_cells, target_cell)


def predict_control(data, c):
    # data = c.recv(1024).decode('utf-8')

    loop = True
    while loop:
        data = recv_msg(c)
        if data != None:
            data = data.decode('utf-8')
            if data.find("GET_PREDICTED"):
                predict(c, data)
            if data == 'PREDICT_END':
                loop = False
            else:
                loop = False


def predict(data, c):
    data_predict_start = data.find(": ")
    data_redict = data[data_predict_start + 2]
    # Parse neighboring cell RSRP from data
    predict_parse(data_redict)
    y = predictRsrp(neighboring_rsrp)
    predicted = "Predicted RSRP: " + y
    send_msg(c, predicted)


def predict_parse(data):
    data_start = data.find("[") + 1
    data_end = data.find("[")

    data_input = data[data_start:data_end]
    data_input_lines = data_input.split(";")
    neighboring_rsrp = []
    index = 0
    for data_in in data_input_lines:
        data_in_start = data.find(",") + 1
        data_in_end = data.find("},")
        if index == 0:
            cell_actual_rsrp = int(data_in[data_in_start, data_in_end])
        else:
            neighboring_rsrp[index - 1] = int(data_in[data_in_start, data_in_end])
        index += 1

    return neighboring_rsrp


def upload_data(data):
    # parse filename and write to file
    # 
    filename = data
    meas_data = data
    f = open(filename, "a")
    f.write(meas_data)
    f.close()


if __name__ == '__main__':
    Main()
